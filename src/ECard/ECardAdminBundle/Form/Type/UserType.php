<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use ECard\ECardBundle\Entity\User;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text');
        $builder->add('email', 'email', array('required' => true));
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'required' => false,
            'first_options' => array(
                'label' => 'Password'
            ),
            'second_options' => array(
                'label' => 'Confirm Password'
            ),
        ));
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();
            $form->add('roles', 'entity', array(
                'class' => 'ECardBundle:Role',
                'property' => 'name',
                'multiple' => true,
                'data' => $data->getRoles()
            ));
        });
        
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ECard\ECardBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'userForm';
    }
}