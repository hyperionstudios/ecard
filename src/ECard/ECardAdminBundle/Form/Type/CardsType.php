<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CardsType extends AbstractType
{
    private $cards;
    
    public function __construct($cards) {
        $this->cards = $cards;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cards', 'entity', array(
                'class' => 'ECardBundle:Card',
                'choices' => $this->cards,
                'property' => 'name',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('add', 'submit');
        $builder->add('edit', 'submit');
        $builder->add('delete', 'submit');
    }


    public function getName()
    {
        return 'cardsForm';
    }
}