<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CardFileType extends AbstractType
{
    private $required;
    
    public function __construct($required = true) {
        $this->required = $required;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
            'required' => false,
            'validation_groups' => array($this->required ? 'registration' : 'default'),
            'error_bubbling' => false,
            'attr' => array(
                'accept' => 'image/*,video/*',
                'novalidate' => 'novalidate'
            ),
            'label' => 'Card Image or Video'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ECard\ECardBundle\Entity\CardFile',
        ));
    }

    public function getName()
    {
        return 'cardFileType';
    }
}

