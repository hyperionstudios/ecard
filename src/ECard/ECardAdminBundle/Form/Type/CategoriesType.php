<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoriesType extends AbstractType
{
    private $categories;
    
    public function __construct($categories) {
        $this->categories = $categories;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('categories', 'entity', array(
                'class' => 'ECardBundle:Category',
                'choices' => $this->categories,
                'property' => 'name',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('add', 'submit');
        $builder->add('edit', 'submit');
        $builder->add('delete', 'submit');
    }


    public function getName()
    {
        return 'categoriesForm';
    }
}