<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ECard\ECardBundle\Form\DataTransformer\TagsTransformer;
use ECard\ECardBundle\Entity\Category;

/**
 * Credit due to Joshua Thijssen 
 * https://www.adayinthelifeof.nl/2014/03/19/dynamic-form-modification-in-symfony2/
 */
class CardType extends AbstractType
{
    private $em;
    
    public function __construct($em) {
        $this->em = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'label' => 'Title',
        ));
        $builder->add('description', 'textarea');
        $builder->add('cardFile', new CardFileType(true)); 
        $builder->add('cardThumb', new CardFileType(false)); 
        $builder->add(
            $builder->create('tags', 'textarea', array(
                'attr' => array('placeholder' => 'Use commas to seperate tags.')
                )
            )->addModelTransformer(new TagsTransformer())
        );
        $builder->add('submit', 'submit');

        // Add listeners
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'onPreSubmit'));
    }
 
 
    protected function addElements(FormInterface $form, Category $category = null) {
 
        // Add the category element
        $form->add('category', 'entity', array(
            'class' => 'ECardBundle:Category',
            'empty_value' => 'Choose Category',
            'property' => 'name',
            'attr' => array(
                'class' => 'form-control'
            ),
            'data' => $category,
            'mapped' => false,
        ));
 
        if (!$category) {
            $catRepo = $this->em->getRepository('ECardBundle:Category');
            //Find the first category!
            $category = $catRepo->findBy(array(), array(), 1)[0];
        }
        
        // Fetch the subcategories from the supplied category
        $subRepo = $this->em->getRepository('ECardBundle:SubCategory');
        $subCategories = $subRepo->findBy(array('category' => $category), array('name' => 'asc'));
        
        $form->add('subcategory', 'entity', array(
            'class' => 'ECardBundle:SubCategory',
            'empty_value' => 'Choose Subcategory',
            'choices' => $subCategories,
            'property' => 'name',
            'attr' => array(
                'class' => 'form-control'
            ),
        ));
        
    }
 
 
    function onPreSubmit(FormEvent $event) {
        $form = $event->getForm();
        $data = $event->getData();
 
        // Note that the data is not yet hydrated into the entity.
        $category = $this->em->getRepository('ECardBundle:Category')->find($data['category']);
        $this->addElements($form, $category);
    }
 
 
    function onPreSetData(FormEvent $event) {
        $card = $event->getData();
        $form = $event->getForm();
 
        // We might have an empty card (when we insert a new card, for instance)
        $category = $card->getSubCategory() ? $card->getSubCategory()->getCategory() : null;
        $this->addElements($form, $category);
    }

    public function getName()
    {
        return 'cardForm';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ECard\ECardBundle\Entity\Card',
            ));
    }
}