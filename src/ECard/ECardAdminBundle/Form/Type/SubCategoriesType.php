<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubCategoriesType extends AbstractType
{
    private $subCategories;
    
    public function __construct($subCategories) {
        $this->subCategories = $subCategories;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('subcategories', 'entity', array(
                'class' => 'ECardBundle:SubCategory',
                'choices' => $this->subCategories,
                'property' => 'name',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('add', 'submit');
        $builder->add('edit', 'submit');
        $builder->add('delete', 'submit');
    }


    public function getName()
    {
        return 'subCategoriesForm';
    }
}