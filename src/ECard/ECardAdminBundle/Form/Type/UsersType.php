<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UsersType extends AbstractType
{
    private $users;
    
    public function __construct($users) {
        $this->users = $users;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('users', 'entity', array(
                'class' => 'ECardBundle:User',
                'choices' => $this->users,
                'property' => 'username',
                'multiple'  => true,
                'expanded' => true,
            ));
        $builder->add('edit', 'submit');
        $builder->add('delete', 'submit');
    }

    public function getName()
    {
        return 'usersForm';
    }
}