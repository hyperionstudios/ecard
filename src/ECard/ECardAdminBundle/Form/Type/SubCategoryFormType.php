<?php
namespace ECard\ECardAdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SubCategoryFormType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('category', 'entity', array(
                'class' => 'ECardBundle:Category',
                'property' => 'name',
                'data' => $options['category'],
                'attr' => array(
                    'class' => 'form-control',
                )
            ));
        $builder->add('name', 'text');
        $builder->add('submit', 'submit');
    }

    public function getName()
    {
        return 'subCategoryForm';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'category' => null,
            'data_class' => 'ECard\ECardBundle\Entity\SubCategory',
        ));
    }
}

?>
