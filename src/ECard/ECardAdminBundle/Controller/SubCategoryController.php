<?php
namespace ECard\ECardAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use ECard\ECardAdminBundle\Form\Type\SubCategoryFormType;
use ECard\ECardAdminBundle\Form\Type\SubCategoriesType;
use ECard\ECardBundle\Entity\SubCategory;

class SubCategoryController extends Controller
{
    public function indexAction(Request $request, $page, $category)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->find('ECardBundle:Category', $category);

        if (!$category) {
            throw $this->createNotFoundException('The category does not exist');
        }
        
        $limit = $this->container->getParameter('maxCardsPerPage');
        $offset = ($page - 1) * $limit;
        
        
        $subCatRepo = $em->getRepository("ECardBundle:SubCategory");
        $subCategories = $subCatRepo->findBy(array('category' => $category), array('id' => 'ASC'), $limit, $offset);
      
        
        $catCount = $subCatRepo->countAll();
        
        $pages = ceil($catCount / $limit);
        
        $form = $this->createForm(new SubCategoriesType($subCategories));
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $selectedCategories = $form->get('subcategories')->getData();
            $cCount = count($selectedCategories);
            if ($form->get('add')->isClicked()) {
                    return $this->redirect($this->generateUrl('ECardAdmin_add_subcategory', array(
                        'category' => $category->getId(),
                        )));
            }
            else if ($cCount >= 1) {
                if ($form->get('edit')->isClicked()) {
                    if ($cCount != 1) {
                        $form->addError(new FormError('You must only select one subcategory.'));
                    }
                    else {     
                        return $this->redirect($this->generateUrl('ECardAdmin_edit_subcategory', array(
                            'id' => $selectedCategories[0]->getId(),
                            'category' => $category->getId(),
                            )));
                    }
                }
                else if ($form->get('delete')->isClicked()) {

                    $catRemoveMsg = '';
                    foreach ($selectedCategories as $sCat) {
                        $em->remove($sCat);

                        $cardsRemoveMsg .= $sCat->getName().', ';    
                    }
                    $em->flush();

                    $this->get('session')->getFlashBag()->set('success', 'Removed '. $catRemoveMsg);
                    return $this->redirect($this->generateUrl('ECardAdmin_subcategories', array(
                        'category' => $category->getId(),
                        )));
                
                }
            }
            else {
                $form->addError(new FormError('You must select a subcategory.'));
            }
           //$em = $this->getDoctrine()->getManager();
           //$server = $form->getData();
        }
   
        return $this->render('ECardAdminBundle:SubCategory:index.html.twig', array(
            'category' => $category,
            'subCategories' => $subCategories, 
            'form' => $form->createView(), 
            'pages' => $pages, 
            'page' => $page,
                ));
    }
    
    public function addAction(Request $request, $category) {
        $em = $this->getDoctrine()->getManager();
        $category = $em->find('ECardBundle:Category', $category);

        if (!$category) {
            throw $this->createNotFoundException('The category does not exist');
        }
            
        $form = $this->createForm(new SubCategoryFormType(), new SubCategory(), array('category' => $category));     
        $form->handleRequest($request);
        if ($form->isValid()) {
            
            $subCategory = $form->getData();
            
            $em->persist($subCategory);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'Added '. $subCategory->getName());
            return $this->redirect($this->generateUrl('ECardAdmin_subcategories', array(
                'category' => $category->getId(),
                )));
        }
        return $this->render('ECardAdminBundle:SubCategory:add.html.twig', array(
            'form' => $form->createView(),
            'category' => $category,
        ));
    }
    
    public function editAction(Request $request, $id, $category) {
        $em = $this->getDoctrine()->getManager();
        $subCategory = $em->find("ECardBundle:SubCategory", $id);
        $category = $em->find('ECardBundle:Category', $category);
        
        if (!$subCategory) {
            throw $this->createNotFoundException('The subcategory does not exist');
        }
        else if (!$category) {
            throw $this->createNotFoundException('The category does not exist');
        }
        
        $form = $this->createForm(new SubCategoryFormType(), $subCategory);     
        $form->handleRequest($request);
        if ($form->isValid()) {
            $subCategory = $form->getData();
            
            $em->persist($subCategory);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'Edited '. $subCategory->getName());
            return $this->redirect($this->generateUrl('ECardAdmin_subcategories', array(
                'category' => $category->getId(),
                )));
        }
        return $this->render('ECardAdminBundle:SubCategory:edit.html.twig', array(
            'form' => $form->createView(), 
            'subCategory' => $subCategory,
            'category' => $category,
        ));
    }
    
    public function ajaxGetCategoryAction(Request $request, $category) {
        $em = $this->getDoctrine()->getManager();
        $category = $em->find('ECardBundle:Category', $category);
        if (!$category) {
            throw $this->createNotFoundException('The category does not exist');
        }
        
        return new JsonResponse($category);
    }
}