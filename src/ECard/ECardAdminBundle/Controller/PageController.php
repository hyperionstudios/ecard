<?php
namespace ECard\ECardAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;


class PageController extends Controller
{
    public function indexAction(Request $request)
    {

        return $this->render('ECardAdminBundle:Page:index.html.twig');
    }
}