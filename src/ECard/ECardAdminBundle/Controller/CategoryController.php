<?php
namespace ECard\ECardAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use ECard\ECardAdminBundle\Form\Type\CategoryFormType;
use ECard\ECardAdminBundle\Form\Type\CategoriesType;

class CategoryController extends Controller
{
    public function indexAction(Request $request, $page)
    {

        $limit = $this->container->getParameter('maxCardsPerPage');
        $offset = ($page - 1) * $limit;
        
        $em = $this->getDoctrine()->getManager();
        $catRepo = $em->getRepository("ECardBundle:Category");
        $categories = $catRepo->findBy(array(), array('id' => 'ASC'), $limit, $offset);
      
        
        $catCount = $catRepo->countAll();
        
        $pages = ceil($catCount / $limit);
        
        $form = $this->createForm(new CategoriesType($categories));
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $selectedCategories = $form->get('categories')->getData();
            $cCount = count($selectedCategories);
            if ($form->get('add')->isClicked()) {
                    return $this->redirect($this->generateUrl('ECardAdmin_add_category'));
            }
            else if ($cCount >= 1) {
                if ($form->get('edit')->isClicked()) {
                    if ($cCount != 1) {
                        $form->addError(new FormError('You must only select one category.'));
                    }
                    else {     
                        return $this->redirect($this->generateUrl('ECardAdmin_edit_category', 
                                array('id' => $selectedCategories[0]->getId())
                                ));
                    }
                }
                else if ($form->get('delete')->isClicked()) {

                    $catRemoveMsg = '';
                    foreach ($selectedCategories as $sCat) {
                        $em->remove($sCat);

                        $cardsRemoveMsg .= $sCat->getName().', ';    
                    }
                    $em->flush();

                    $this->get('session')->getFlashBag()->set('success', 'Removed '. $catRemoveMsg);
                    return $this->redirect($this->generateUrl('ECardAdmin_categories'));
                
                }
            }
            else {
                $form->addError(new FormError('You must select a category.'));
            }
           //$em = $this->getDoctrine()->getManager();
           //$server = $form->getData();
        }
   
        return $this->render('ECardAdminBundle:Category:index.html.twig', array(
            'categories' => $categories, 'form' => $form->createView(), 'pages' => $pages, 'page' => $page
                ));
    }
    
    public function addAction(Request $request) {
        
        $form = $this->createForm(new CategoryFormType());     
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $category = $form->getData();
            
            $em->persist($category);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'Added '. $category->getName());
            return $this->redirect($this->generateUrl('ECardAdmin_categories'));
        }
        return $this->render('ECardAdminBundle:Category:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $category = $em->find("ECardBundle:Category", $id);
        if (!$category) {
            throw $this->createNotFoundException('The card does not exist');
        }
        
        $form = $this->createForm(new CategoryFormType(), $category);     
        $form->handleRequest($request);
        if ($form->isValid()) {
            $category = $form->getData();
            
            $em->persist($category);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'Edited '. $category->getName());
            return $this->redirect($this->generateUrl('ECardAdmin_categories'));
        }
        return $this->render('ECardAdminBundle:Category:edit.html.twig', array(
            'form' => $form->createView(), 
            'category' => $category,
        ));
    }
}