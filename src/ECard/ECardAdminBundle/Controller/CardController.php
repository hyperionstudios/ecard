<?php
namespace ECard\ECardAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use ECard\ECardAdminBundle\Form\Type\CardsType;
use ECard\ECardAdminBundle\Form\Type\CardType;
use ECard\ECardBundle\Entity\Card;

class CardController extends Controller
{
    public function indexAction(Request $request, $page, $category, $subcategory, $name, $sortby, $tags)
    {
        //convert tags into an array
        $tags = explode(',', $tags);
        
        $limit = $this->container->getParameter('maxCardsPerPage');
        $offset = ($page - 1) * $limit;
        
        $em = $this->getDoctrine()->getManager();
        $cardRepo = $em->getRepository("ECardBundle:Card");
        //$cards = $cardRepo->findBy(array(), array(), $limit, $offset);
        
        $search = array(
            'name' => $name,
            'category' => $category,
            'subcategory' => $subcategory,
            'sortby' => $sortby,
            'tags' => $tags,
            );
        
        $cards = $cardRepo->findBySearchWithLimit($offset, $limit, $search);
        
        $cardCount = $cardRepo->countAll($search);
        
        $pages = ceil($cardCount / $limit);
        
        $form = $this->createForm(new CardsType($cards));
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $selectedCards = $form->get('cards')->getData();
            $cCount = count($selectedCards );
            if ($form->get('add')->isClicked()) {
                    return $this->redirect($this->generateUrl('ECardAdmin_add_card'));
            }
            else if ($cCount >= 1) {
                if ($form->get('edit')->isClicked()) {
                    if ($cCount != 1) {
                        $form->addError(new FormError('You must only select one card.'));
                    }
                    else {     
                        return $this->redirect($this->generateUrl('ECardAdmin_edit_card', 
                                array('id' => $selectedCards[0]->getId())
                                ));
                    }
                }
                else if ($form->get('delete')->isClicked()) {

                    $cardsRemoveMsg = '';
                    foreach ($selectedCards as $sCard) {
                        $em->remove($sCard);

                        $cardsRemoveMsg .= $sCard->getName().', ';    
                    }
                    $em->flush();

                    $this->get('session')->getFlashBag()->set('success', 'Removed '. $cardsRemoveMsg);
                    return $this->redirect($this->generateUrl('ECardAdmin_cards'));
                
                }
            }
            else {
                $form->addError(new FormError('You must select a card.'));
            }
           //$em = $this->getDoctrine()->getManager();
           //$server = $form->getData();
        }
   
        return $this->render('ECardAdminBundle:Card:index.html.twig', array(
            'cards' => $cards, 'form' => $form->createView(), 'pages' => $pages, 'page' => $page
                ));
    }
    
    public function addAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        
        //populate a default list of subCategories
        $form = $this->createForm(new CardType($em), new Card());     
        $form->handleRequest($request);
        if ($form->isValid()) {
            
            $card = $form->getData();
            //print_r($card);
            $em->persist($card);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'Added '. $card->getName());
            return $this->redirect($this->generateUrl('ECardAdmin_cards'));
        }
        return $this->render('ECardAdminBundle:Card:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    public function editAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $card = $em->find("ECardBundle:Card", $id);
        if (!$card) {
            throw $this->createNotFoundException('The card does not exist');
        }
        
        $form = $this->createForm(new CardType($em), $card);     
        $form->handleRequest($request);
        if ($form->isValid()) {
            $card = $form->getData();
            
            $em->persist($card);
            $em->flush();
            
            $this->get('session')->getFlashBag()->set('success', 'Edited '. $card->getName());
            return $this->redirect($this->generateUrl('ECardAdmin_cards'));
        }
        return $this->render('ECardAdminBundle:Card:edit.html.twig', array(
            'form' => $form->createView(), 
            'card' => $card,
        ));
    }
}