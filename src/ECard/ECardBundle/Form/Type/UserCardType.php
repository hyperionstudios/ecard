<?php
namespace ECard\ECardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'textarea', array(
            'attr' => array(
                'placeholder' => 'Mininum 35 characters',
                ),
        ));
        
        $builder->add('use_login', 'checkbox', array(
            'required' => false,
            'label' => 'Send as login account?',
            'mapped' => false,
        ));
        //used to get a receiver's user account if they are registered
        $builder->add('receiver', 'text',array(
            'label' => 'To a Registered User',
            'required' => false,
            'mapped' => false,
        ));   
        $builder->add('senderEmail', 'email', array(
            'required' => false,
            'label' => 'From Email',
            'attr' => array(
                'placeholder' => 'example@example.com',
            ),
        )); 
        $builder->add('receiverEmail', 'email', array(
            'required' => false,
            'label' => 'To Email',
            'attr' => array(
                'placeholder' => 'example@example.com',
            ),
        )); 
        $builder->add('senderName', 'text', array(
            'required' => false,
            'label' => 'From',
            'attr' => array(
                'placeholder' => 'Your name or alias',
            ),
        )); 
        $builder->add('receiverName', 'text', array(
            'required' => false,
            'label' => 'To',
            'attr' => array(
                'placeholder' => 'The receiver\'s name or alias',
            ),
        )); 
        $builder->add('public', 'checkbox', array(
            'required' => false,
            'label' => 'Show this card pubicly?',
        )); 
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ECard\ECardBundle\Entity\UserCard'
        ));
    }

    public function getName()
    {
        return 'userCardForm';
    }
}