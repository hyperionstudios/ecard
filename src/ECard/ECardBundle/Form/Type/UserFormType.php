<?php
namespace ECard\ECardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text');
        $builder->add('email', 'email', array('required' => true));
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'required' => false,
            'first_options' => array(
                'label' => 'Password'
            ),
            'second_options' => array(
                'label' => 'Confirm Password'
            ),
        ));
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ECard\ECardBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'userForm';
    }
}