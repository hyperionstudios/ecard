<?php
namespace ECard\ECardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ECard\ECardBundle\Form\Type\CardSearchType;

class UserCardSearchType extends AbstractType
{
    private $sortByOptions;
    
    public function __construct(array $sortByOptions = null) {
        if (empty($sortByOptions)) {
            $sortByOptions = array(
                'receiver' => 'name',
                'subcategory' => 'subcategory',
                );
        }
        $this->sortByOptions = $sortByOptions;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('card', new CardSearchType($this->sortByOptions), $options);
        
        $builder->add('email', 'text', array(
            'required' => false,
            'attr' => array(
                'class' => 'form-control',
                'placeholder' => 'Search by email',
            ),
            'empty_data' => 'none',
        ));
        
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'categories' => array(),
            'subcategories' => array(),
            'category_readonly' => false,
        ));
    }
    
    public function getName()
    {
        return 'userCardSearchForm';
    }
    
}