<?php
namespace ECard\ECardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use ECard\ECardBundle\Form\DataTransformer\TagsTransformer;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CardSearchType extends AbstractType
{
    private $sortByOptions;
    private $firstCat = null;
    
    public function __construct(array $sortByOptions = array()) {
        if (empty($sortByOptions)) {
            $sortByOptions = array(
                'name' => 'name',
                'subcategory' => 'subcategory',
                );
        }
        $this->sortByOptions = $sortByOptions;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (count($options['categories']) == 1) {
            $this->firstCat = $options['categories'][0];
        }
        $builder->add('category', 'entity', array(
            'required' => false,
            'class' => 'ECardBundle:Category',
            'property' => 'name',
            'attr' => array(
                'class' => 'form-control'
            ),
            'empty_value' => 'Search By Category',
            'choices' => $options['categories'],
            'data' => $this->firstCat,
            'read_only' => $options['category_readonly'],
        ));  
        $builder->add('subcategory', 'entity', array(
            'required' => false,
            'class' => 'ECardBundle:SubCategory',
            'property' => 'name',
            'attr' => array(
                'class' => 'form-control'
            ),
            'choices' => $options['subcategories'],
            'empty_value' => 'Search By SubCategory',
            'empty_data' => 'all',
        ));
        $builder->add('name', 'text', array(
            'required' => false,
            'attr' => array(
                'placeholder' => 'Search by name',
            ),
            'empty_data' => 'none',
        ));  
        
        $builder->add('sortBy', 'choice', array(
            'required' => false,
            'attr' => array(
                 'class' => 'form-control'
            ),
            'empty_value' => 'Sort By',
            'empty_data' => 'default',
            'choices' => $this->sortByOptions,
        ));
        $builder->add(
            $builder->create('tags', 'text', array(
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Search by Tags; Use commas to seperate tags.'
                    ),
                'empty_data' => 'none'
                ))->addModelTransformer(new TagsTransformer())
        );
  
        $builder->add('submit', 'submit');
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'categories' => array(),
            'subcategories' => array(),
            'category_readonly' => false,
        ));
    }
    
    public function getName()
    {
        return 'cardSearchForm';
    }
    
}