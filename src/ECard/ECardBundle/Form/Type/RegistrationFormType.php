<?php
namespace ECard\ECardBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('user', new UserFormType());
        $builder->add('termsAccepted', 'checkbox',array(
            'label' => 'Terms of Service',
            ));
        $builder->add('recaptcha', 'ewz_recaptcha',array(
                'attr' => array(
                    'options' => array(
                        'theme' => 'white'
                    ))
            ));
    }

    public function getName()
    {
        return 'registration';
    }
}

?>
