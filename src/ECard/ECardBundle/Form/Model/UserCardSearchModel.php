<?php
namespace ECard\ECardBundle\Form\Model;

use ECard\ECardBundle\Form\Model\CardSearchModel;

class UserCardSearchModel extends CardSearchModel
{
    protected $email;
    
    protected $card;

    public function setEmail($email) {
        $this->email = $email;
    }
    public function getEmail() {
        return $this->email;
    }
    
    public function setCard($card) {
        $this->card = $card;
    }
    public function getCard() {
        return $this->card;
    }
    
}

