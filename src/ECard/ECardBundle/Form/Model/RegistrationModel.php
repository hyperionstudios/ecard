<?php
namespace ECard\ECardBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;

use ECard\ECardBundle\Entity\User;

class RegistrationModel
{
    /**
     * @Assert\Type(type="ECard\ECardBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @Assert\True(message="If you agree to the ToS, this must be checked.")
     */
    protected $termsAccepted;

    /**
    * @Recaptcha\True(message="The value you entered did not match the captcha.")
    */
    protected $recaptcha;
    
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = (Boolean) $termsAccepted;
    }
    
    public function getRecaptcha() {
        return $this->recaptcha;
    }
    
    public function setRecaptcha($recaptcha) {
        $this->recaptcha = $recaptcha;
    }
}
