<?php
namespace ECard\ECardBundle\Form\Model;

use ECard\ECardBundle\Entity\Category;

class CardSearchModel
{
    protected $category;
    
    protected $subCategory;
    
    protected $name;
    
    protected $tags;

    protected $sortBy;
    
    public function setCategory($category) {
        $this->category = $category;
    }
    
    /**
     * Gets the category's name
     * @return string
     */
    public function getCategory() {
        $catName = 'all';
        if ($this->category) {
            $catName = $this->category->getName();
        }
        return $catName;
    }
    
    public function setSubCategory($subCategory) {
        $this->subCategory = $subCategory;
    }
    
    /**
     * Gets the category's name
     * @return string
     */
    public function getSubCategory() {
        $catName = 'all';
        if ($this->subCategory) {
            $catName = $this->subCategory->getName();
        }
        return $catName;
    }

    public function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }
    
    public function setTags($tags) {
        $this->tags = $tags;
    }
    public function getTags() {
        return $this->tags;
    }
    
    public function setSortBy($sortBy) {
        $this->sortBy = $sortBy;
    }
    
    public function getSortBy() {
        return $this->sortBy;
    }
    
}

