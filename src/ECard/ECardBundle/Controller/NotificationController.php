<?php
namespace ECard\ECardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;


class NotificationController extends Controller
{
    /**
     * 
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxNotificationsAction(Request $request)
    {
        $user = $this->getUser();
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $userCardRepo = $em->getRepository('ECardBundle:UserCard');
            
            $notifications = $userCardRepo->findUnread($user);

            $json = array();
            foreach ($notifications as $notification) {
                $json[] = array(
                    'card' => array(
                        'id' => $notification->getCard()->getId(),
                        'name' => $notification->getCard()->getName(),
                        ),
                    'sender' => array(
                        'id' => $notiication->getSender()->getUsername(),
                        'name' => $notification->getSender()->getUsername(),
                    ),
                );
            }
            
            //print_r($json);
            
            return new JsonResponse($json);
        }
        
        throw $this->createNotFoundException('Requires user to login!'); 
    }
}