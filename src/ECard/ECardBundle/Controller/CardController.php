<?php
namespace ECard\ECardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use ECard\ECardBundle\Form\Type\UserCardType;
use ECard\ECardBundle\Entity\UserCard;
use ECard\ECardBundle\Entity\User;

class CardController extends Controller {
    
    public function beginSendAction(Request $request, $id) {    
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('ECardBundle:User');
        
        $card = $em->find('ECardBundle:Card', $id);
        if (!$card) {
            throw $this->createNotFoundException('The card does not exist');
        }
        
        $form = $this->createForm(new UserCardType(), new UserCard());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $userCard = $form->getData();
            $username = $form->get('receiver')->getData();
            $valid = true;
            $receiver = null;

            if (!empty($username)) {
                $receiver = $userRepo->findBy(array('username' => $username));
                if (count($receiver) == 0) {
                    $form->get('receiver')->addError(new FormError('That user doesn\'t exist'));
                    $receiver = "null"; //we don't want other validation to bother trying.
                    $valid = false;
                }
                else {
                    //we don't want arrays!
                    $receiver = $receiver[0];
                }
            }

            if (!$receiver) {
                if (empty($userCard->getReceiverEmail()) && !$userCard->isPublic()) {
                    $form->addError(new FormError('You must enter a user or an email so I know where to send this card to.'));
                    $valid = false;
                }
                if (empty($userCard->getReceiverName())) {
                    $form->get('receiverName')->addError(new FormError('You must enter a name or alias for the receiver.'));
                    $valid = false;
                }
            }

            //no errors please!
            if ($valid) {
                if ($this->getUser() && $form->get('use_login')->getData() === true) {
                    //User might not be logged in! 
                    //and user choosen to assoicate their account with it
                    $userCard->setSenderUser(new User()); 
                    // why bother sending a real user object?  
                    // doesn't carry over sessions anyway....
                }
                if ($receiver) {
                    //User added a user receiver.
                    $userCard->setReceiverUser($receiver);
                }

                if (empty($userCard->getSenderName())) {
                    $userCard->setSenderName('Anonymous');
                }

                $userCard->setCard($card);

                $em->persist($userCard); //Persist so lifecycle events will be fired
                $em->detach($userCard); //We must detach before serailizing to a session

                $session = $request->getSession();
                $session->set('userCard', $userCard);

                return $this->redirect($this->generateUrl('ECard_preview_card'));
            }

        }
        return $this->render('ECardBundle:Card:send.html.twig', array(
            'card' => $card,
            'form' => $form->createView(),
            ));
    }
    
    public function previewAction(Request $request) {
        $session = $request->getSession();
        $userCard = $session->get('userCard');
        if (!$userCard) {
            throw $this->createNotFoundException('Your user card preview has expired.');
        }
        
        $em = $this->getDoctrine()->getManager();
        $userCard = $em->merge($userCard); //Don't flush before detaching this object!!!

        //This is a hack work around with some weird ass problem 
        //where the user object isn't passed corectly across sessions.....
        if ($userCard->getSenderUser()) {
            $userCard->setSenderUser($this->getUser());
        }
        
        //var_dump($userCard->getReceiverUser());
        return $this->render('ECardBundle:Card:preview.html.twig', array(
                'card' => $userCard->getCard(),
                'userCard' => $userCard,
                ));
    }
    
    public function finalSendAction(Request $request) {
        $session = $request->getSession();
        $userCard = $session->get('userCard');
        if (!$userCard && !($userCard instanceof UserCard)) {
            throw $this->createNotFoundException('Your user card has expired.');
        }
        
        $em = $this->getDoctrine()->getManager();
        $userCard = $em->merge($userCard); //Merge to be managed again by doctrine
        
        //This is a hack work around with some weird ass problem 
        //where the user object isn't passed corectly across sessions.....
        if ($userCard->getSenderUser()) {
            $userCard->setSenderUser($this->getUser());     
        }
        
        
        //throw new \Exception($userCard->getSenderUser()->getUsername());
        if ($userCard->getReceiverEmail()) {
            $this->emailAction($request, $userCard, $userCard->getReceiverEmail()); 
        }
        
        if ($userCard->getReceiverUser() 
                && $userCard->getReceiverEmail() !== $userCard->getReceiverUser()->getEmail()) {
            //no point in sending to the same email twice
            $this->emailAction($request, $userCard, $userCard->getReceiverUser()->getEmail());
        }
        
        
        $em->flush();
        $id = $userCard->getUniqueId();
        $session->remove('userCard');
        
        $this->get('session')->getFlashBag()->set('success', 'Card has been sent');
        return $this->redirect($this->generateUrl('ECard_private_view', array(
            'id' => $id,
        )));
        /*return $this->render('ECardBundle:Email:card.html.twig', array(
                'card' => $userCard->getCard(),
                'userCard' => $userCard,
                ));*/
    }
    
    
    public function viewAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $userCardRepo = $em->getRepository('ECardBundle:UserCard');
        
        $userCard = $userCardRepo->findByUniqueId($id);
        
        if (!$userCard) {
            throw $this->createNotFoundException('Your user card doesn\'t exist.');
        }
        /*if (!$userCard->isPublic()) {
            //We don't want to reveal the existence of this private card.
            throw $this->createNotFoundException('Your user card doesn\'t exist.');
        }*/

        return $this->render('ECardBundle:Card:view.html.twig', array(
            'card' => $userCard->getCard(),
            'userCard' => $userCard,
            ));
    }
    
    /**
     * This is not routeable!
     */
    public function emailAction(Request $request, $userCard, $to) {
        //We need to refresh the usercard's card memory!
        $em = $this->getDoctrine()->getManager();
        $userCard->setCard($em->find('ECardBundle:Card', $userCard->getCard()->getId()));
        
        $from = $this->container->getParameter('mailer_default_address');

        $message = \Swift_Message::newInstance()
            ->setSubject('Someone has sent you an e-card!')
            ->setFrom($from)
            ->setTo($to)
            ->setContentType('text/html') 
            ->setBody(
                $this->renderView(
                    'ECardBundle:Email:card.html.twig',
                    array(
                        'card' => $userCard->getCard(),
                        'userCard' => $userCard,
                    )
                ));
        $this->get('mailer')->send($message);

    }
    
    /**
     * This is just used for testing
     */
    public function testEmailAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $userCard = $em->find('ECardBundle:UserCard', 3);
        
        //$session = $request->getSession();
        //$userCard = $session->get('userCard');
        
        return $this->render('ECardBundle:Email:card.html.twig', array(
            'card' => $userCard->getCard(),
            'userCard' => $userCard,
        ));
    }
    
    
}
