<?php
namespace ECard\ECardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use ECard\ECardBundle\Form\Type\CardSearchType;
use ECard\ECardBundle\Form\Type\UserCardSearchType;
use ECard\ECardBundle\Form\Model\CardSearchModel;
use ECard\ECardBundle\Form\Model\UserCardSearchModel;

class SearchController extends Controller {
     public function searchBarAction(Request $request, $route, $category, $readOnly) {
        $em = $this->getDoctrine()->getManager();
        $catRepo = $em->getRepository('ECardBundle:Category');
        $subRepo = $em->getRepository('ECardBundle:SubCategory');
        
        if ($category === "all") {
            $categories = $catRepo->findAll();
            $subCategories = $subRepo->findAll();
        }
        else {
            $categories = $catRepo->findBy(array('name' => $category));
            if (count($categories) > 0) {
                $subCategories = $subRepo->findBy(array('category' => $categories[0]));
            }
        }

        $form = $this->createForm(new CardSearchType(), new CardSearchModel(), array(
            'categories' => $categories,
            'subcategories' => $subCategories,
            'category_readonly' => !strcmp($readOnly, "readonly"),
        )) ;
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $search = $form->getData();
             
            //var_dump($search);
            return $this->redirect($this->generateUrl($route, array(
                'tags' => implode(',', $search->getTags()),
                'name' => $search->getName(),
                'category' => $search->getCategory(),
                'subcategory' => $search->getSubCategory(),
                'sortby' => $search->getSortBy(),
            )));
        }

        return $this->render('ECardBundle:Form:searchCard.html.twig', 
                array(
                    'form' => $form->createView(),
                    'route' => $route,
                    'category' => $category,
                    'readOnly' => $readOnly,
                ));
    }
    
    public function publicSearchBarAction(Request $request, $route, $category, $readOnly) {
        $em = $this->getDoctrine()->getManager();
        $catRepo = $em->getRepository('ECardBundle:Category');
        $subRepo = $em->getRepository('ECardBundle:SubCategory');
        
        if ($category === "all") {
            $categories = $catRepo->findAll();
            $subCategories = $subRepo->findAll();
        }
        else {
            $categories = $catRepo->findBy(array('name' => $category));
            if (count($categories) > 0) {
                $subCategories = $subRepo->findBy(array('category' => $categories[0]));
            }
        }

        $form = $this->createForm(new UserCardSearchType(), new UserCardSearchModel(), array(
            'categories' => $categories,
            'subcategories' => $subCategories,
            'category_readonly' => !strcmp($readOnly, "readonly"),
        )) ;
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $search = $form->getData();
             
            //var_dump($search);
            return $this->redirect($this->generateUrl($route, array(
                'tags' => implode(',', $search->getTags()),
                'name' => $search->getName(),
                'email' => $search->getEmail(),
                'category' => $search->getCategory(),
                'subcategory' => $search->getSubCategory(),
                'sortby' => $search->getSortBy(),
            )));
        }

        return $this->render('ECardBundle:Form:searchUserCard.html.twig', 
                array(
                    'form' => $form->createView(),
                    'route' => $route,
                    'category' => $category,
                    'readOnly' => $readOnly,
                ));
    }
}
