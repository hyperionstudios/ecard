<?php
namespace ECard\ECardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;


class PageController extends Controller
{
    public function indexAction(Request $request, $page, $category, $subcategory, $name, $sortby, $tags)
    {
        $em = $this->getDoctrine()->getManager();
        
        //convert tags into an array
        $tags = explode(',', $tags);
                
        $maxCardsOnPage = $this->container->getParameter('maxCardsPerPage');
        $offset = ($page - 1) * $maxCardsOnPage;
        
        $cardRepo = $em->getRepository('ECardBundle:Card');
        
        $search = array(
            'name' => $name,
            'category' => $category,
            'subcategory' => $subcategory,
            'sortby' => $sortby,
            'tags' => $tags,
            );
        
        $cards = $cardRepo->findBySearchWithLimit($offset, $maxCardsOnPage, $search);
        
        $cardCount = $cardRepo->countAll($search);
        
        $pages = ceil($cardCount / $maxCardsOnPage);
        
        $allTags = $cardRepo->getTags();
        $tagWeights = $cardRepo->getTagWeights($allTags);
        
        $search['tags'] = implode(',', $tags);
        
        return $this->render('ECardBundle:Page:index.html.twig', array(
            'page' => $page,
            'cards' => $cards,
            'pages' => $pages,
            'tags' => $allTags,
            'tagWeights' => $tagWeights,
            'query' => $search,
        ));
    }
    
    public function publicAction(Request $request, $page, $category, $subcategory, $name, $email, $sortby, $tags)
    {
        //convert tags into an array
        $tags = explode(',', $tags);
        
        $em = $this->getDoctrine()->getManager();
        $maxCardsOnPage = $this->container->getParameter('maxCardsPerPage');
        $offset = ($page - 1) * $maxCardsOnPage;
        
        $userCardRepo = $em->getRepository('ECardBundle:UserCard');
        
        $search = array(
            'name' => $name,
            'email' => $email,
            'category' => $category,
            'subcategory' => $subcategory,
            'sortby' => $sortby,
            'tags' => $tags,
            );
        
        $userCards = $userCardRepo->findPublic($offset, $maxCardsOnPage, $search);
        
        $cardCount = $userCardRepo->countAll($search);
        
        $pages = ceil($cardCount / $maxCardsOnPage);
       
        $search['tags'] = implode(',', $tags);
        
        return $this->render('ECardBundle:Page:public.html.twig', array(
            'page' => $page,
            'userCards' => $userCards,
            'pages' => $pages,
            'query' => $search,
        ));
    }
    
    public function baseNavbarLinkAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('ECardBundle:Category')->findAll();
        
        return $this->render('::baseNavbarLink.html.twig', array(
            'categories' => $categories,
        ));
    }
}