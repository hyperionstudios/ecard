<?php
namespace ECard\ECardBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\ORM\EntityManager;
use Liip\ThemeBundle\ActiveTheme;
use Symfony\Component\HttpFoundation\Cookie;

class KernelListener
{
    protected $em;
    protected $theme;
    protected $cookieOptions;
    protected $cookie;
    
    function __construct(EntityManager $em, ActiveTheme $theme, array $cookieOptions)
    {
        $this->em = $em;
        $this->theme = $theme;
        $this->cookieOptions = $cookieOptions;
    }
    
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
        $request = $event->getRequest();

        $category = null;

        if ($request->attributes->has('category') === true) {
            $category = $request->attributes->get('category');
        }
        else {
            $category = $request->cookies->get('category_theme');
        }
        
        if (!empty($category)) {     
            if ($category !== "all") {
                $catRepo = $this->em->getRepository('ECardBundle:Category');
                $cat = $catRepo->findOneBy(array('name' => $category));
                if ($cat) {
                    $this->theme->setName($cat->getTheme());
                }
            }
            
            $this->cookie = new Cookie(
                'category_theme',
                $category,
                time() + $this->cookieOptions['lifetime'],
                $this->cookieOptions['path'],
                $this->cookieOptions['domain'],
                (Boolean) $this->cookieOptions['secure'],
                (Boolean) $this->cookieOptions['http_only']
            );
        }

    }
    
    public function onKernelResponse(FilterResponseEvent $event) {
        $response = $event->getResponse();
        if ($this->cookie) {
            $response->headers->setCookie($this->cookie);
        }
    }
}