<?php
namespace ECard\ECardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="ecard_user_cards")
 * @ORM\Entity(repositoryClass="ECard\ECardBundle\Entity\Repository\UserCardRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserCard
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $uniqueId;
    
    /**
     * @ORM\ManyToOne(targetEntity="Card", inversedBy="userCards", cascade={"persist", "remove"})
     */
    protected $card;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="sentCards")
     * @ORM\JoinColumn(name="senderUser_id", referencedColumnName="id", nullable=true)
     */
    protected $senderUser = null;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="receivedCards")
     * @ORM\JoinColumn(name="recieverUser_id", referencedColumnName="id", nullable=true)
     */
    protected $receiverUser = null;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $receiverEmail;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $senderEmail;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $receiverName;
    
    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $senderName;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *  min=35,
     *  minMessage = "Message must be at least {{ limit }} characters long."
     * )
     */
    protected $message;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $public = false;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $hasRead = false;


    /**
     * @ORM\Column(type="datetime")
     */
    protected $sentDate;
    
    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get unique id
     *
     * @return string
     */
    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    /**
     * Sets card message
     * @param String $message
     */
    public function setMessage($message) {
        $this->message= $message;
    }
    /**
     * Get card message
     * 
     * @return String 
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function setCard($card) {
        $this->card = $card;
    }
    /**
     * Get card
     * 
     * @return Card
     */
    public function getCard()
    {
        return $this->card;
    }
    
    /**
     * Sets the sender user
     * 
     * @param User $senderUser
     */
    public function setSenderUser($senderUser) {
        $this->senderUser = $senderUser;
    }
    
    /**
     * Gets the sender user
     * 
     * @return User
     */
    public function getSenderUser() {
        return $this->senderUser;
    }
    
    /**
     * Sets the receiving user
     * 
     * @param User $receiverUser
     */
    public function setReceiverUser($receiverUser) {
        $this->receiverUser = $receiverUser;
    }
    
    /**
     * Gets the receving user
     * 
     * @return User
     */
    public function getReceiverUser() {
        return $this->receiverUser;
    }
    
    /**
     * Get sent date
     * 
     * @return Datetime
     */
    
    public function getSentDate() {
        return $this->sentDate;
    }

    /**
     * Set receiverEmail
     *
     * @param string $receiverEmail
     * @return UserCard
     */
    public function setReceiverEmail($receiverEmail)
    {
        $this->receiverEmail = $receiverEmail;

        return $this;
    }

    /**
     * Get receiverEmail
     *
     * @return string 
     */
    public function getReceiverEmail()
    {
        return $this->receiverEmail;
    }

    /**
     * Set senderEmail
     *
     * @param string $senderEmail
     * @return UserCard
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;

        return $this;
    }

    /**
     * Get senderEmail
     *
     * @return string 
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * Set receiverName
     *
     * @param string $receiverName
     * @return UserCard
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;

        return $this;
    }

    /**
     * Get receiverName
     *
     * @return string 
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     * @return UserCard
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;

        return $this;
    }

    /**
     * Get senderName
     *
     * @return string 
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Set Public
     *
     * @param boolean $public
     * @return UserCard
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get Public
     *
     * @return boolean 
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * Set read
     *
     * @param boolean $hasRead
     * @return UserCard
     */
    public function setRead($hasRead)
    {
        $this->hasRead = $hasRead;

        return $this;
    }

    /**
     * Get read
     *
     * @return boolean 
     */
    public function hasRead()
    {
        return $this->hasRead;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function userCardPrePersist() {
        $this->sentDate = new \DateTime();
        //This should be as unique as possible!
        //Using base36 for short unique urls
        $this->uniqueId = base_convert(uniqid($this->getReceiverName() . mt_rand(0,10000), true), 10, 36);
    }
}
