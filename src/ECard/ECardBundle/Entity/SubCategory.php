<?php
namespace ECard\ECardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Table(name="ecard_subcategories")
 * @ORM\Entity(repositoryClass="ECard\ECardBundle\Entity\Repository\CategoryRepository")
 */
class SubCategory implements \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="subCategories")
     */
    protected $category;
    
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\Length(
     *  min=3,
     *  max=25,
     *  minMessage = "Name must be at least {{ limit }} characters",
     *  maxMessage = "Name name cannot be longer than {{ limit }} characters"
     * ) 
     * @Assert\NotBlank()
     */
    protected $name;
    
    /**
     * @ORM\OneToMany(targetEntity="Card", mappedBy="subCategory")
     */
    protected $cards;

    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setCategory($category) {
        $this->category = $category;
    }
    
    public function getCategory() {
        return $this->category;
    }
    
    /**
     * Sets card title
     * @param String $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Get card title
     * 
     * @return String 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function getCards() {
        return $this->cards;
    }
    
    public function jsonSerialize() {
        $json = array(
            'id' => $this->getId(),
            'name' => $this->getName(),
        );
        return $json;
    }
}