<?php
namespace ECard\ECardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Table(name="ecard_cards")
 * @ORM\Entity(repositoryClass="ECard\ECardBundle\Entity\Repository\CardRepository")
 * @UniqueEntity(fields="name", message="Name has already been used.")
 * @ORM\HasLifecycleCallbacks()
 */
class Card
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="SubCategory", inversedBy="cards")
     */
    protected $subCategory;
    
    /**
     * @ORM\OneToMany(targetEntity="UserCard", mappedBy="card", cascade={"persist", "remove"})
     */
    protected $userCards;
            
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\Length(
     *  min=3,
     *  max=25,
     *  minMessage = "Name must be at least {{ limit }} characters",
     *  maxMessage = "Name name cannot be longer than {{ limit }} characters"
     * ) 
     * @Assert\NotBlank()
     */
    protected $name;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\Length(
     *  min=35,
     *  minMessage = "Description must be at least {{ limit }} characters long."
     * )
     * @Assert\NotBlank(message="Description should not be blank.")
     */
    protected $description;
    
    /**
     * @ORM\OneToOne(targetEntity="CardFile", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    protected $cardFile;
    
    /**
     * @ORM\OneToOne(targetEntity="CardFile", cascade={"persist", "remove"})
     */
    protected $cardThumb = null;
   
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;
    
    /**
     * @ORM\Column(type="simple_array", nullable=true)
     * @Assert\All({
     *      @Assert\NotBlank(message="Tags should not be blank."),
     *      @Assert\Length(
     *          min=3,
     *          max=20,
     *          minMessage = "Tags must be at least {{ limit }} characters long.",
     *          maxMessage = "Tags must be no longer than {{ limit }} characters."
     *      ),
     * })
     * @Assert\NotNull(message="You need at least one tag!")
     */
    protected $tags;
    

    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setSubCategory($subcategory) {
        $this->subCategory = $subcategory;
    }
    
    public function getSubCategory() {
        return $this->subCategory;
    }
    
    public function setUserCards($userCards) {
        $this->userCards = $userCards;
    }
    
    public function getUserCards() {
        return $this->userCards;
    }
    
    /**
     * Sets card title
     * @param String $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    /**
     * Get card title
     * 
     * @return String 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets card description
     * @param String $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }
    /**
     * Get card description
     * 
     * @return String 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function setCardFile($cardFile) {
        $this->cardFile = $cardFile;
    }
    /**
     * Get card image
     * 
     * @return CardFile
     */
    public function getCardFile()
    {
        return $this->cardFile;
    }
    
    public function setCardThumb($cardThumb) {
        $this->cardThumb = $cardThumb;
    }
    /**
     * Get card image
     * 
     * @return CardFile
     */
    public function getCardThumb()
    {
        return $this->cardThumb;
    }
    
    /**
     * Get creation date
     * 
     * @return Datetime
     */
    
    public function getCreationDate() {
        return $this->creationDate;
    }
    
    /**
     * Set tags
     *
     * @param array $tags
     * @return Card
     */
    public function setTags($tags)
    {
        //force tags to be lowercased.
        foreach ($tags as $key => $value) {
            $tags[$key] = strtolower($value);
        }
        //Make sure no dupilcate tags are in the array.
        $this->tags = array_unique($tags);
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return array 
     */
    public function getTags()
    {
        return $this->tags;
    }
   
    /**
     * @ORM\PrePersist
     */
    public function cardPrePersist() {
        $this->creationDate = new \DateTime();
    }
    
}