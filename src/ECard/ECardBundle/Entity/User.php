<?php
namespace ECard\ECardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Table(name="ecard_users")
 * @ORM\Entity(repositoryClass="ECard\ECardBundle\Entity\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="Email already been used.")
 * @UniqueEntity(fields="username", message="Username already been used.")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\Length(
     *  min=3,
     *  max=25,
     *  minMessage = "Username must be at least {{ limit }} characters",
     *  maxMessage = "Username name cannot be longer than {{ limit }} characters"
     * ) 
     * @Assert\NotBlank()
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $password;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $creationDate;

    /**
     *
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email(checkMX = true)
     */
    protected $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

     /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     *
     */
    protected $roles;
    
    /**
     * @ORM\OneToMany(targetEntity="UserCard", mappedBy="senderUser")
     */
    protected $sentCards;
    
    /**
     * @ORM\OneToMany(targetEntity="UserCard", mappedBy="receiverUser")
     */
    protected $receivedCards;
    
    public function __construct()
    {
        $this->isActive = true;
        $this->roles = new ArrayCollection();
        $this->servers = new ArrayCollection();
    }

    public function setUsername($username) {
        $this->username = $username;
    }
    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

   public function setEmail($email) {
        $this->email = $email;
    }
    /**
     * @inheritDoc
     */
    public function getEmail()
    {
        return $this->email;
    }

    /* We atm have no use for salts */
    public function setSalt($salt) {
        //$this->salt = $salt;
    }
    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        //return $this->salt;
        return null;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
    /**
     * @inheritDoc
     */

    public function getPassword()
    {
        return $this->password;
    }
    /**
     * @inheritDoc
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    public function setPlainPassword($password) {
        $this->plainPassword = $password;
    }

    public function setRoles($roles) {
        $this->roles = $roles;
    }
    public function addRole($role) {
        $this->roles->add($role);
    }
    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        if ($this->roles == null) {
            return array();
        }
        return $this->roles->toArray();
    }
        /**
     * Remove roles
     *
     * @param \ProjectGxp\GxpBundle\Entity\Role $roles
     */
    public function removeRole($roles)
    {
        $this->roles->removeElement($roles);
    }
    

   public function setServers($servers) {
        $this->servers = $servers;
    }
    public function addServers($servers) {
        $this->servers->add($servers);
    }
     /**
     * Remove servers
     *
     * @param \ProjectGxp\GxpBundle\Entity\Server $servers
     */
    public function removeServer($servers)
    {
        $this->servers->removeElement($servers);
    }
    /**
     * @inheritDoc
     */
    public function getServers()
    {
        if ($this->servers == null) {
            return array();
        }
        return $this->servers->toArray();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
        ) = unserialize($serialized);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get creation date
     * 
     * @return Datetime
     */
    
    public function getCreationDate() {
        return $this->creationDate;
    }
    
    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add servers
     *
     * @param Server $servers
     * @return User
     */
    public function addServer($servers)
    {
        $this->servers[] = $servers;
    
        return $this;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function userPrePersist() {
        $this->creationDate = new \DateTime();
    }
    
}