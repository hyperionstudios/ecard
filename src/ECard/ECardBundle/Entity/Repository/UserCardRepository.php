<?php
namespace ECard\ECardBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;
use ECard\ECardBundle\Entity\Category;

class UserCardRepository extends EntityRepository {
    
    public function countAll(array $search = null) {
        $query = $this->createQueryBuilder('c')->select('COUNT(c.id)');
        if (!empty($search)) { 
            $query = $this->getQueryForSearchParameters($query, $search);
        }
        return $query->getQuery()->getSingleScalarResult();
    }
    
     /**
     * @return QueryBuilder
     */
    public function getQueryForSearchParameters(QueryBuilder $query, $search) {
        $query->innerJoin('c.card', 'card');
        if (!empty($search['tags']) && $search['tags'][0] !== "none") {
            foreach ($search['tags'] as $key => $tag) {
                $query->andWhere('c.tags LIKE :tags'.$key)
                    ->setParameter('tags'.$key,'%'.$tag.'%');
            }
        }
        if (!empty($search['name']) && $search['name'] !== "none") {
            $query->andWhere('c.receiverName LIKE :name')
                ->setParameter('name', '%'.$search['name'].'%'); //append wildcards to the ends
        }
        if (!empty($search['email']) && $search['email'] !== "none") {
            $query->andWhere('c.receiverEmail LIKE :email')
                ->setParameter('email', '%'.$search['email'].'%'); //append wildcards to the ends
        }
        if (!empty($search['subcategory']) && $search['subcategory'] !== "all") {
            $query->innerJoin('card.subCategory', 'sub', Join::WITH, 'sub.name = :subcategory')
                ->setParameter('subcategory', $search['subcategory']);
        }
        if (!empty($search['category']) && $search['category'] !== "all") {
            //Begin some compilicated ass SQL magic......
            
            //Get category's id
            $catQuery = $this->getEntityManager()->createQueryBuilder()
                ->select('cat')
                ->from('ECardBundle:Category', 'cat')
                ->where('cat.name = :category');
            
            //get all subcategories in the category
            $subQuery = $this->getEntityManager()->createQueryBuilder()
                ->select('sub_2')
                ->from('ECardBundle:SubCategory', 'sub_2')
                ->where($query->expr()->in('sub_2.category', $catQuery->getDQL()));
            
            $query->andWhere($query->expr()->in('card.subCategory', $subQuery->getDQL()))
                // We apply our parameter here because it doesn't work in the subqueries
                ->setParameter('category', $search['category']); 
            //echo $query->getQuery()->getSQL();
        }
        
        if (!empty($search['sortby'])) {
            switch ($search['sortby']) {
                case "name":
                    $query->addOrderBy('c.receiverName', 'ASC');
                    break;
                case "subcategory":
                    $query->addOrderBy('card.subcategory', 'ASC');
                    break;
            }
        }

        return  $query;
    }
    
    /** unused */
    public function findUnread($user) {
        $query = $this->createQueryBuilder('c')
                ->select('c')
                ->leftJoin('ECardBundle:User', 'u')
                ->where('u = :user')
                ->setParameter('user', $user);
        
        return $query->getQuery()->getResult();
    }
    
    public function setAsRead($userCard) {
        $query = $this->createQueryBuilder('c')
                ->update()
                ->set('read', true);
        
        return $query->getQuery()->execute();     
    }
   
    public function findPublic($offset, $limit, $search = array()) {
        $query = $this->createQueryBuilder('c')
            ->where('c.public = TRUE')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        if (!empty($search)) { 
            $query = $this->getQueryForSearchParameters($query, $search);
        }
        
        return $query->getQuery()->getResult();
    }
    
    public function findByUniqueId($id) {
        $query = $this->createQueryBuilder('c')
            ->where('c.uniqueId = :id')
            ->setParameter('id', $id);
        
        return $query->getQuery()->getSingleResult();
    }

}
