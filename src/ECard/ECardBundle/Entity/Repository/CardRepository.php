<?php
namespace ECard\ECardBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;


class CardRepository extends EntityRepository {
    
    public function countAll(array $search = null) {
        $query = $this->createQueryBuilder('c')->select('COUNT(c.id)');
        if (!empty($search)) { 
            $query = $this->getQueryForSearchParameters($query, $search);
        }
        return $query->getQuery()->getSingleScalarResult();
    }

    
    /**
     * @return QueryBuilder
     */
    public function getQueryForSearchParameters(QueryBuilder $query, $search) {
        if (!empty($search['tags']) && $search['tags'][0] !== "none") {
            foreach ($search['tags'] as $key => $tag) {
                $query->andWhere('c.tags LIKE :tags'.$key)
                    ->setParameter('tags'.$key,'%'.$tag.'%');
            }
        }
        if (!empty($search['name']) && $search['name'] !== "none") {
            $query->andWhere('c.name LIKE :name')
                    ->setParameter('name', '%'.$search['name'].'%'); //append wildcards to the ends
        }
        if (!empty($search['subcategory']) && $search['subcategory'] !== "all") {
            $query->innerJoin('c.subCategory', 'sub', Join::WITH, 'sub.name = :subcategory')
                    ->setParameter('subcategory', $search['subcategory']);
        }
        if (!empty($search['category']) && $search['category'] !== "all") {
            //Begin some compilicated ass SQL magic......
            
            //Get category's id
            $catQuery = $this->getEntityManager()->createQueryBuilder()
                ->select('cat')
                ->from('ECardBundle:Category', 'cat')
                ->where('cat.name = :category');
            
            //get all subcategories in the category
            $subQuery = $this->getEntityManager()->createQueryBuilder()
                ->select('ssub')
                ->from('ECardBundle:SubCategory', 'ssub')
                ->where($query->expr()->in('ssub.category', $catQuery->getDQL()));
            
            $query->andWhere($query->expr()->in('c.subCategory', $subQuery->getDQL()))
                // We apply our parameter here because it doesn't work in the subqueries
                ->setParameter('category', $search['category']); 
            //echo $query->getQuery()->getSQL();
        }
        
        if (!empty($search['sortby'])) {
            switch ($search['sortby']) {
                case "name":
                    $query->addOrderBy('c.name', 'ASC');
                    break;
                case "category":
                    $query->addOrderBy('c.category', 'ASC');
                    break;
            }
        }

        return  $query;
    }
    
    public function findBySearchWithLimit($offset, $max, array $search) {
         $query = $this->createQueryBuilder('c')
            ->select('c')
            ->setFirstResult($offset)
            ->setMaxResults($max);
        if (!empty($search)) { 
            $query = $this->getQueryForSearchParameters($query, $search);
        }
        
        //echo $query->getQuery()->getSQL();
        return $query->getQuery()->getResult();
    }
    
    public function getTags() {
        $cardTags = $this->createQueryBuilder('c')
                         ->select('c.tags')
                         ->getQuery()
                         ->getResult();
        // print_r($cardTags);
        $tags = array();
        foreach ($cardTags as $cardTag)
        {
            $tags = array_merge($cardTag['tags'], $tags);
        }

        foreach ($tags as &$tag)
        {
            $tag = trim($tag);
        }

        return $tags;
    }

    public function getTagWeights($tags) {
        $tagWeights = array();
        if (empty($tags)) {
            return $tagWeights;
        }
        
        foreach ($tags as $tag)
        {
            $tagWeights[$tag] = (isset($tagWeights[$tag])) ? $tagWeights[$tag] + 1 : 1;
        }
        // Shuffle the tags
        uksort($tagWeights, function() {
            return rand() > rand();
        });

        $max = max($tagWeights);

        // Max of 5 weights
        $multiplier = ($max > 5) ? 5 / $max : 1;
        foreach ($tagWeights as &$tag)
        {
            $tag = ceil($tag * $multiplier);
        }

        return $tagWeights;
    }

}
