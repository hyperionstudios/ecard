<?php
namespace ECard\ECardBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository {
    
    public function countAll() {
        $query = $this->createQueryBuilder('u')->select('COUNT(u.id)');

        return $query->getQuery()->getSingleScalarResult();
    }
    
    public function findUsersLikeNameAndLimit($username, $limit) {
        $query = $this->createQueryBuilder('u')
            ->select('u.username')
            ->andWhere('u.username LIKE :username')
            ->setMaxResults($limit)
            ->setParameter('username', $username .'%');
        
        $users = $query->getQuery()->getArrayResult();
        $cleanUsers = array();
        foreach($users as $user) {
            $cleanUsers[] = $user['username'];
        }
        
        return $cleanUsers;
    }

}
