<?php
namespace ECard\ECardBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class CategoryRepository extends EntityRepository {
    
    public function countAll() {
        $query = $this->createQueryBuilder('c')->select('COUNT(c.id)');
        
        return $query->getQuery()->getSingleScalarResult();
    }

}
