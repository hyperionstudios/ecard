<?php
namespace ECard\ECardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Table(name="ecard_categories")
 * @ORM\Entity(repositoryClass="ECard\ECardBundle\Entity\Repository\CategoryRepository")
 * @UniqueEntity(fields="name", message="Name has already been used.")
 */
class Category implements \JsonSerializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\Length(
     *  min=3,
     *  max=25,
     *  minMessage = "Name must be at least {{ limit }} characters",
     *  maxMessage = "Name name cannot be longer than {{ limit }} characters"
     * ) 
     * @Assert\NotBlank()
     */
    protected $name;
    
    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\Length(
     *  min=3,
     *  max=25,
     *  minMessage = "Theme Name must be at least {{ limit }} characters",
     *  maxMessage = "Them name cannot be longer than {{ limit }} characters"
     * ) 
     * @Assert\NotBlank()
     */
    protected $theme;

    /**
     * @ORM\OneToMany(targetEntity="SubCategory", mappedBy="category")
     */
    protected $subCategories;
    
    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Sets card title
     * @param String $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Get card title
     * 
     * @return String 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function setSubCategories($subCategories) {
        $this->subCategories = $subCategories;
    }
    
    public function getSubCategories() {
        return $this->subCategories;
    }
    public function jsonSerialize() {
        $json = array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'subcategories' => array(),
        );
        foreach ($this->subCategories as $sub) {
            $json['subcategories'][] = $sub->jsonSerialize();
        }
        
        return $json;
    }

    /**
     * Set theme
     *
     * @param string $theme
     * @return Category
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Add subCategories
     *
     * @param \ECard\ECardBundle\Entity\SubCategory $subCategories
     * @return Category
     */
    public function addSubCategory(\ECard\ECardBundle\Entity\SubCategory $subCategories)
    {
        $this->subCategories[] = $subCategories;

        return $this;
    }

    /**
     * Remove subCategories
     *
     * @param \ECard\ECardBundle\Entity\SubCategory $subCategories
     */
    public function removeSubCategory(\ECard\ECardBundle\Entity\SubCategory $subCategories)
    {
        $this->subCategories->removeElement($subCategories);
    }
}
